/* ===========================================
 * LevPherCMS gophermap engine
 * by Nathaniel Leveck
 * tested on Arch Linux (current, Sept 2017)
 * and Raspbian (current Sept 2017)
 * Requires the mySql or MariaDB connector 
 * libraries - change the server and login
 * information for your server on lines 25-26
 * ===========================================
 * 
 *  This program is free software: you can 
 *  redistribute it and/or modify it under the 
 *  terms of the GNU General Public License as 
 *  published by the Free Software Foundation, 
 *  either version 3 of the License, or (at 
 *  your option) any later version.
 *
 *  This program is distributed in the hope 
 *  that it will be useful, but WITHOUT ANY 
 *  WARRANTY; without even the implied warranty 
 *  of MERCHANTABILITY or FITNESS FOR A 
 *  PARTICULAR PURPOSE.  See the GNU General 
 *  Public License for more details.
 *
 *  You should have received a copy of the 
 *  GNU General Public License along with 
 *  this program.  If not, see 
 *  <http://www.gnu.org/licenses/>.
 */

#include </usr/include/mysql/my_global.h>
#include </usr/include/mysql/mysql.h>
#include <string.h>
#include <unistd.h>

int main(int argc, char **argv)
{  
char *srv = "localhost";
char *usr = "user";
char *pwd = "password";
char *dbn = "gopherCMS";
char *sectionSQL;
int c;
char *menu_mode;
char *sectionLine = "______________________________________________________________________";

	MYSQL *con = mysql_init(NULL);
	if (con == NULL) 
	{
		fprintf(stderr, "%s\n", mysql_error(con));
		exit(1);
	}
	if (mysql_real_connect(con, srv, usr, pwd, dbn, 0, NULL, 0) == NULL) 
	{
		fprintf(stderr, "%s\n", mysql_error(con));
		mysql_close(con);
		exit(1);
	}

	MYSQL *conn = mysql_init(NULL);
	if (conn == NULL)
	{
	fprintf(stderr, "%s\n", mysql_error(conn));
		exit(1);
	}
	if (mysql_real_connect(conn, srv, usr, pwd, dbn, 0, NULL, 0) == NULL)
	{
		fprintf(stderr, "%s\n", mysql_error(conn));
		mysql_close(conn);
		exit(1);
	}

/*
 * the page table needs to have at least pageText (longtext) and pageID (int)
 * fields. I have pageID as an autoincrement primary index
 */ 

/* Uncomment if you have text you want to display from the page table
	if (mysql_query(con, "SELECT pageText FROM `page` WHERE pageId='1';")) 
	{
		printf("Query failed: %s\n", mysql_error(con));
	}
	else 
	{
		MYSQL_RES *result = mysql_store_result(con);
		if (!result) 
		{
			printf("Couldn't get results set: %s\n", mysql_error(con));
		} 
		else 
		{
			MYSQL_ROW row;
			int i;
			unsigned int num_fields = mysql_num_fields(result);
			while ((row = mysql_fetch_row(result))) 
			{
				for (i = 0; i < num_fields; i++) 
				{
					printf("%s", row[i]);
				}
				putchar('\n');
			}
			mysql_free_result(result);
		}
	}
 */

/*
 * the section table needs to have the following fields:
 * sortType varchar(10) - either abcOrdered or manual
 * title    varchar(50) - section title
 * secSort  float       - sort order
 * pageID   int         - refers to the page table
 */
	asprintf(&sectionSQL, "%s", "SELECT sortType, title, id  FROM section WHERE `pageID`='1' ORDER BY `secSort` ASC;");
	if (mysql_query(con, sectionSQL))
        {
                printf("Query failed: %s\n", mysql_error(con));
        }
        else
        {
                MYSQL_RES *resultSec = mysql_store_result(con);
                if (!resultSec)
                {
                        printf("Couldn't get results set: %s\n", mysql_error(con));
                }
                else
                {
                        MYSQL_ROW sec;
			int x;
                        while ((sec = mysql_fetch_row(resultSec)))
                        {
                                printf(" \n[%s]%s\n", sec[1], sectionLine);
/*
 * the links table requires the following:
 * linkText varchar(255) - text to place above links in the gophermap
 * linkType varchar(1)   - gopher link type
 * linkName varchar(255) - the actual text displayed as a link
 * linkPath varchar(255) - path of the gopher link
 * linkHost varchar(255) - host you're linking to
 * linkPort varchar(6)   - port the linked-to gopher server is running on
 * linkSort float        - sort order
 * pageID int            - refers back to the page table
 */
				char *tmp;
				if (strcmp(sec[0],"abcOrdered")) 
				{
					asprintf(&tmp, "%s%s%s", "SELECT linkText, linkType, linkName, linkPath, linkHost, linkPort FROM links WHERE `pageID`='1' AND `section`='", sec[2], "'  ORDER BY `linkSort` ASC;");
				}
				else
				{
					asprintf(&tmp, "%s%s%s", "SELECT linkText, linkType, linkName, linkPath, linkHost, linkPort FROM links WHERE `pageID`='1' AND `section`='", sec[2], "'  ORDER BY `linkName` ASC;");
				}
				if (mysql_query(conn, tmp))
				{
					printf("Query failed: %s\n", mysql_error(conn));
				} 
				else 
				{
				MYSQL_RES *result = mysql_store_result(conn);
				if (!result) 
				{
					printf("Couldn't get results set: %s\n", mysql_error(conn));
				} 
				else 
				{
					MYSQL_ROW row;
					while ((row = mysql_fetch_row(result))) 
					{
						printf("%s%s\t%s\t%s\t%s%s\n", row[1], row[2], row[3], row[4], row[5], row[0]);
					}
				}
				}
			tmp=NULL;
                        }
                }
		mysql_close(conn);
                mysql_close(con);
                exit(0);
        }
}
